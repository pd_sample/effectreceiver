#include "RecvDmxThread.h"
#include <QUdpSocket>


#define ART_NET_PORT 6454

RecvDmxThread::RecvDmxThread(QObject *parent)
    : QThread(parent)
{
    is_exit = false;
}

RecvDmxThread::~RecvDmxThread()
{
    is_exit = true;
}

void RecvDmxThread::run() {
    qRegisterMetaType<QNetworkDatagram>();

    QUdpSocket recv_socket;
    recv_socket.bind(QHostAddress::LocalHost, ART_NET_PORT);

    while (!is_exit) {
        if (recv_socket.hasPendingDatagrams()) {
            QNetworkDatagram data = recv_socket.receiveDatagram();
            emit DmxDataReceived(data);
        }
    }


}
