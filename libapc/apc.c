#include "apc.h"

#include <stdlib.h>

#include "udp.h"
#include "polarssl/aes.h"

#define PACKET_MAXLEN (9216)
#define MAGIC_NUM (0xDEADC0DE)

#define HEX(X) (((X)>='a')?((X)-'a'+10):(((X)>='A')?((X)-'A'+10):((X)-'0')))

struct apc_sender {
    struct udp_conn *h;
    aes_context ctx;
    int id;
};
struct apc_receiver {
    struct udp_conn *h;
    aes_context ctx;
    unsigned char rx[PACKET_MAXLEN];
};

struct apc_sender *apc_sender_new(
    int self_id, const char *target_ip, int target_port, 
    const char *bind_ip, const char *target_key_hex) {
    int i;
    char t1, t2;
    unsigned char key[16];
    struct udp_conn *h = NULL;
    struct apc_sender *s = NULL;

    h = udp_conn_new(target_ip, target_port, bind_ip, -1);
    if (!h)
        return NULL;

    s = (struct apc_sender*) malloc(sizeof(struct apc_sender));
    if (!s) {
        udp_conn_close(h);
        return NULL;
    }
    s->h = h;

    for (i = 0; i< 16; ++i) {
        t1 = target_key_hex[i*2];
        t2 = target_key_hex[i*2+1];
        key[15-i] = (unsigned char)((HEX(t1) << 4) | HEX(t2));
    }
    aes_setkey_enc(&s->ctx, key, 128);
    s->id = self_id;
    return s;
}
void apc_sender_del(struct apc_sender *h) {
    if (!h)
        return;
    udp_conn_close(h->h);
    free(h);
}

struct apc_receiver *apc_receiver_new(const char *bind_ip, int self_listen_port, const char *self_key_hex) {
    int i;
    char t1, t2;
    unsigned char key[16];
    struct udp_conn *h = NULL;
    struct apc_receiver *r = NULL;

    h = udp_conn_new(NULL, -1, bind_ip, self_listen_port);
    if (!h)
        return NULL;

    r = (struct apc_receiver*)malloc(sizeof(struct apc_receiver));
    if (!r) {
        udp_conn_close(h);
        return NULL;
    }
    r->h = h;

    for (i = 0; i< 16; ++i) {
        t1 = self_key_hex[i*2];
        t2 = self_key_hex[i*2+1];
        key[15-i] = (unsigned char)((HEX(t1) << 4) | HEX(t2));
    }
    aes_setkey_dec(&r->ctx, key, 128);
    return r;
}
void apc_receiver_del(struct apc_receiver *h) {
    if (!h)
        return;
    udp_conn_close(h->h);
    free(h);
}


int apc_sender_send(struct apc_sender *h, struct apc_msg_hdr_t *msg) {
    unsigned char tx[PACKET_MAXLEN];
    unsigned char iv[16] = {0};
    int len, ret;

    if (h->id < 0)
        return -1;

    len = ((msg->len - 1) / 16 + 1) * 16;
    if (len > PACKET_MAXLEN - 4)
        return 0;

    msg->magic = MAGIC_NUM;
    msg->id = h->id;

    ret = aes_crypt_cbc(&h->ctx, AES_ENCRYPT, len, iv, (unsigned char *)msg, tx);
    if (ret != 0)
        return 0;

    return udp_conn_send(h->h, (const char *)tx, len);
}


int apc_receiver_recv(struct apc_receiver *h, int timeout_us, struct apc_msg_hdr_t **pmsg) {
    int c, ret = 0;
    struct apc_msg_hdr_t *msghdr;
    unsigned char iv[16] = {0};

    if (pmsg)
        *pmsg = NULL;

    c = udp_conn_recv(h->h, h->rx, PACKET_MAXLEN, timeout_us, NULL, NULL);
    if (c == 0)
        return 0;
    if (c < 0)
        return -1;
    if (c % 16)
        return 0;

    ret = aes_crypt_cbc(&h->ctx, AES_DECRYPT, c, iv, h->rx, h->rx);
    if (ret != 0)
        return 0;

    msghdr = (struct apc_msg_hdr_t *)h->rx;
    if (msghdr->magic != MAGIC_NUM)
        return 0;

    if (pmsg) *pmsg = msghdr;
    return c;
}

