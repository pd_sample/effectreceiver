#ifndef LIBAPC_SERIAL_H
#define LIBAPC_SERIAL_H

#ifdef __cplusplus
extern "C" {
#endif

struct term_conn;

struct term_conn *term_conn_new(const char *dev, int baud, int databit, char parity, int stopbit);

void term_conn_close(struct term_conn *h);

// return: number of bytes sent.
//         < 0 error.
//         = 0 timeout.
int term_conn_send(struct term_conn *h, const char *buf, int buflen);

// timeout_us:  -1: blocking.
//             = 0: return immediately.
// return: number of bytes readed.
//         < 0 error.
//         = 0 timeout,
int term_conn_recv(struct term_conn *h, unsigned char *buf, int buflen, long timeout_us);

#ifdef __cplusplus
}
#endif

#endif // LIBAPC_SERIAL_H

