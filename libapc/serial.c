#include "serial.h"

#ifdef _WIN32
//#define  _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:4127)
#include <winsock2.h>
#include <mstcpip.h>
#else
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>

#endif

struct term_conn {
#ifdef _WIN32
    HANDLE h;
#else
    int h;
#endif
};


struct term_conn *term_conn_new(const char *dev, int baud, int databit, char parity, int stopbit) {
    struct term_conn r, *res = NULL;
#ifdef _WIN32
    DCB dcb;
    COMMTIMEOUTS CommTimeOuts;
    DWORD dwErrors;
    COMSTAT ComStat;

    r.h = CreateFileA(dev, GENERIC_READ|GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if ((!r.h) || (r.h == INVALID_HANDLE_VALUE))
        return NULL;

    if (GetCommState(r.h, &dcb) <= 0) {
        CloseHandle(r.h);
        return NULL;
    }

    dcb.BaudRate = baud;
    dcb.ByteSize = (BYTE)databit;

    switch (parity) {
        case 'O': dcb.Parity = ODDPARITY; break;
        case 'E': dcb.Parity = EVENPARITY; break;
        case 'N':
        default: dcb.Parity = NOPARITY; break;
    }

    switch (stopbit) {
        case 2:  dcb.StopBits = TWOSTOPBITS; break;
        case 1:
        default: dcb.StopBits = ONESTOPBIT; break;
    }

    dcb.fBinary         = TRUE;                     // binary mode, no EOF check
    dcb.fOutxCtsFlow    = FALSE;                    // CTS output flow control
    dcb.fOutxDsrFlow    = FALSE;                    // DSR output flow control
    dcb.fDtrControl     = DTR_CONTROL_HANDSHAKE;    // DTR flow control type
    dcb.fDsrSensitivity = FALSE;                    // DSR sensitivity
    dcb.fOutX           = FALSE;                    // XON/XOFF out flow control
    dcb.fInX            = FALSE;                    // XON/XOFF in flow control
    dcb.fNull           = FALSE;                    // enable null stripping
    dcb.fRtsControl     = RTS_CONTROL_HANDSHAKE;    // RTS flow control
    dcb.fAbortOnError   = FALSE;                    // abort reads/writes on error

    if (SetCommState(r.h, &dcb) < 0) {
        CloseHandle(r.h);
        return NULL;
    }

    CommTimeOuts.ReadIntervalTimeout = 0;
    CommTimeOuts.ReadTotalTimeoutMultiplier = 0;
    CommTimeOuts.ReadTotalTimeoutConstant = 0;
    CommTimeOuts.WriteTotalTimeoutMultiplier = 0;
    CommTimeOuts.WriteTotalTimeoutConstant = 0;
    SetCommTimeouts(r.h, &CommTimeOuts);

    ClearCommError(r.h, &dwErrors, &ComStat);

    SetupComm(r.h, baud+4800, baud+4800);

    res = (struct term_conn *)malloc(sizeof(struct term_conn));
    memcpy(res, &r, sizeof(r));

#else
    struct termios options;

    r.h = open(dev, O_RDWR | O_NOCTTY);
    if (r.h < 0)
        return NULL;

    tcgetattr(r.h, &options);

    options.c_cflag |= CLOCAL;
    options.c_cflag |= CREAD;

    options.c_cflag &= ~CBAUD;
    switch (baud) {
        case 4800:    cfsetispeed(&options, B4800);    cfsetospeed(&options, B4800);    break;
        case 9600:    cfsetispeed(&options, B9600);    cfsetospeed(&options, B9600);    break;
        case 19200:   cfsetispeed(&options, B19200);   cfsetospeed(&options, B19200);   break;
        case 38400:   cfsetispeed(&options, B38400);   cfsetospeed(&options, B38400);   break;
        case 57600:   cfsetispeed(&options, B57600);   cfsetospeed(&options, B57600);   break;
        case 115200:  cfsetispeed(&options, B115200);  cfsetospeed(&options, B115200);  break;
        case 576000:  cfsetispeed(&options, B576000);  cfsetospeed(&options, B576000);  break;
        case 1152000: cfsetispeed(&options, B1152000); cfsetospeed(&options, B1152000); break;
        default: close(r.h); return NULL;
    }

    options.c_cflag &= ~CSIZE;
    switch (databit) {
        case 5: options.c_cflag |= CS5; break;
        case 6: options.c_cflag |= CS6; break;
        case 7: options.c_cflag |= CS7; break;
        case 8:
        default:  options.c_cflag |= CS8; break;
    }

    switch (parity) {
        case 'O': options.c_iflag |= INPCK;  options.c_cflag |= PARENB; options.c_cflag |= PARODD; break;
        case 'E': options.c_iflag |= INPCK;  options.c_cflag |= PARENB; options.c_cflag &= ~PARODD; break;
        case 'N':
        default:  options.c_iflag &= ~INPCK; options.c_cflag &= ~PARENB; break;
    }

    switch (stopbit) {
        case 2: options.c_cflag |= CSTOPB; break;
        case 1:
        default:  options.c_cflag &= ~CSTOPB; break;
    }

    // no hardward flow control
    options.c_cflag &= ~CRTSCTS;
    // no hardward flow control
    options.c_iflag = IGNPAR;
    options.c_iflag &= ~(IXON | IXOFF | IXANY);
    // raw input
    options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
    // raw output
    options.c_oflag &= ~OPOST;

    // following setting should not use with O_NDELAY or O_NONBLOCK
    // if VTIME = 0 and VMIN = 1, it will blocking read
    options.c_cc[VTIME]  = 0; // unit: 1/10 sec
    options.c_cc[VMIN]  = 1;

    tcflush(r.h, TCIFLUSH);
    tcsetattr(r.h, TCSANOW, &options);

    res = (struct term_conn *)malloc(sizeof(struct term_conn));
    memcpy(res, &r, sizeof(r));
#endif
    return res;
}

void term_conn_close(struct term_conn *h) {
    if (!h)
        return;

    if (h->h) {
#ifdef _WIN32
        CloseHandle(h->h);
        h->h = NULL;
#else
        close(h->h);
        h->h = 0;
#endif
    }

    free(h);
}

int term_conn_send(struct term_conn *h, const char *buf, int buflen) {
#ifdef _WIN32
    DWORD r = 0;
    if (FALSE == WriteFile(h->h, buf, buflen, &r, NULL))
        return -1;
#else
    int r = write(h->h, buf, buflen);
    if (r < 0 && (errno == EAGAIN || errno == EWOULDBLOCK))
        return 0;
#endif
    return r;
}

int term_conn_recv(struct term_conn *h, unsigned char *buf, int buflen, long timeout_us) {
    int r;
#ifdef _WIN32
    COMMTIMEOUTS to;
    GetCommTimeouts(h->h, &to);
    if (to.ReadTotalTimeoutConstant != (DWORD)timeout_us) {
        to.ReadIntervalTimeout = MAXDWORD;
        to.ReadTotalTimeoutMultiplier = MAXDWORD;
        to.ReadTotalTimeoutConstant = timeout_us;
        SetCommTimeouts(h->h, &to);
    }
    if (FALSE == ReadFile(h->h, buf, buflen, (LPDWORD)&r, NULL))
        return -1;
#else
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(h->h, &fds);

    if (timeout_us < 0) {
        r = read(h->h, buf, buflen);
    } else {
        struct timeval tv;
        tv.tv_sec  = timeout_us / 1000000;
        tv.tv_usec = timeout_us % 1000000;
        r = select(h->h+1, &fds, 0, 0, &tv);
        if (r <= 0)
            return r; // timeout or error
        r = read(h->h, buf, buflen);
    }
    if (r < 0 && (errno == EAGAIN || errno == EWOULDBLOCK))
        return 0;
    if (r == 0)
        return -1; // socket closed
#endif
    return r;
}

