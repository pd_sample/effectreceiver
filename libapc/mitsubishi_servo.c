#include "mitsubishi_servo.h"

#include "io.h"

#include <string.h>
#include <stdint.h>

#ifdef _WIN32
#include <windows.h>
#else
#include <sys/time.h>
#endif

#define CMD_R_MOTO_SPEED	(0x01),(0x81)
#define CMD_R_ABS_COUNTER	(0x01),(0x8C)
#define CMD_R_SOFTWARE_VER	(0x02),(0x70)

#define CMD_R_ALM_CURR		(0x02),(0x00)
#define CMD_R_ALM_MOST_RECENT	(0x33),(0x10)

#define CMD_R_EXT_INPUT		(0x12),(0x40)
#define CMD_R_EXT_OUTPUT	(0x12),(0xC0)

#define CMD_R_P_STATION_NUM	(0x05),(15)
#define CMD_R_P_STATUS_DISPLAY	(0x05),(18)

#define CMD_W_ALM_RESET		(0x82),(0x00)
#define CMD_W_ALM_HISTORY_CLEAR	(0x82),(0x20)

#define CMD_W_PARAM		(0x82)

#define SOH	(0x01)
#define STX	(0x02)
#define ETX	(0x03)
#define EOT	(0x04)

#define BUF_LEN (128)

// 300 ms
#define WAIT_RECV (300000)

#ifdef _WIN32
#define u_sleep(us) for(;;) { struct timeval v; v.tv_sec=0; v.tv_usec=(us); select(0, 0, 0, 0, &v); break; }
#else
#define u_sleep(us) do{ struct timeval v; v.tv_sec=0; v.tv_usec=(us); select(0, 0, 0, 0, &v); } while(0)
#endif

char D[] = {
    '0','1','2','3','4','5','6','7','8','9',
    'A','B','C','D','E','F','G','H','I','J',
    'K','L','M','N','O','P','Q','R','S','T',
    'U','V','W','X','Y','Z'
};
#define R(x) (((x)<'A')?((x)-'0'):((x)-'A'+10))

static int servo_send(HIO hio, int station, int command, int data_no, char *data, int data_len) {
    int i, sum = 0;
    char buf[BUF_LEN] = {0};
    buf[0] = SOH;
    buf[1] = D[station];
    buf[2] = D[(command >> 4) & 0x0f];
    buf[3] = D[command & 0x0f];
    buf[4] = STX;
    buf[5] = D[(data_no >> 4) & 0x0f];
    buf[6] = D[data_no & 0x0f];
    if (data_len > 0)
        memcpy(buf+7, data, data_len);
    buf[7+data_len] = ETX;

    for (i = 1; i< 8+data_len; ++i)
        sum += buf[i];

    buf[8+data_len] = D[(sum >> 4) & 0x0f];
    buf[9+data_len] = D[sum & 0x0f];

    return io_send(hio, buf, 10+data_len);
}

static int servo_recv(HIO hio, unsigned char *buf, int buflen, int *station, char *errCode) {
    int idx = 0, i, j, r, sum;

    for (;;) {
        r = io_recv(hio, (unsigned char *)buf+idx, buflen-idx, WAIT_RECV);
        if (r <= 0)
            return r;
        idx += r;
        if (buf[0] != STX) {
            for (i = 0; i< idx; ++i)
                if (buf[i] == STX)
                    for (j = i; j< idx; ++j)
                        buf[j-i] = buf[j];
            if (buf[0] != STX) {
                idx = 0;
                continue;
            }
        }
        if (buf[idx-3] != ETX)
            continue;

        // checksum
        sum = 0;
        for (i = 1; i< idx-2; ++i)
            sum += buf[i];
        if ((buf[idx-2] != D[(sum >> 4) & 0x0f]) || (buf[idx-1] != D[sum & 0x0f])) {
            // checksum error, drop data
            buf[0] = 0;
            idx = 0;
            continue;
        }

        if (station) *station = R(buf[1]);
        if (errCode) *errCode = buf[2];
        return idx;
    }
}

static int servo_call(HIO hio, int station, int command, int data_no, char *data, int data_len, unsigned char *buf, int buflen, char *errCode) {
    int i = 0, r;
    int sta;
    char err;
    for (i = 0; i< 4; ++i) { // retry 4 times
        if (i > 0)
            u_sleep(100000);

        r = servo_send(hio, station, command, data_no, data, data_len);
        if (r < 0)
            return r;

        r = servo_recv(hio, buf, buflen, &sta, &err);
        if (r < 0)
            return r;

        if (r == 0)
            continue;
        if (sta != station)
            continue;
        if (err == 'F' || err == 'f')
            continue;

        if (errCode) *errCode = err;
        return r;
    }
    return 0; // timeout
}

static double data_trans_12(unsigned char *data) {
    double ret;
    int i;
    int32_t r = 0;

    for (i = 0; i< 8; ++i) {
        r <<= 4;
        r |= R(data[4+i]);
    }

    ret = r;
    if (data[2] != '0') {
        ret /= R(data[2]);
    }

    return ret;
}

int servo_get_curr_alarm(HIO hio, int station, int *alarm) {
    int ret;
    unsigned char buf[128];
    char err;

    ret = servo_call(hio, station, CMD_R_ALM_CURR, NULL, 0, buf, 128, &err);
    if (ret <= 0)
        return ret;
    if (ret != 10)
        return -1;

    if (alarm) *alarm = (R(buf[5]) << 4 | R(buf[6]));

    return ret;
}

int servo_get_moto_speed(HIO hio, int station, double *speed) {
    int ret;
    unsigned char buf[128];
    char err;

    ret = servo_call(hio, station, CMD_R_MOTO_SPEED, NULL, 0, buf, 128, &err);
    if (ret <= 0)
        return ret;
    if (ret != 18)
        return -1;

    if (speed) *speed = data_trans_12(buf+3);

    return ret;
}

int servo_get_abs_counter(HIO hio, int station, double *count) {
    int ret;
    unsigned char buf[128];
    char err;

    ret = servo_call(hio, station, CMD_R_ABS_COUNTER, NULL, 0, buf, 128, &err);
    if (ret <= 0)
        return ret;
    if (ret != 18)
        return -1;

    if (count) *count = data_trans_12(buf+3);

    return ret;
}

