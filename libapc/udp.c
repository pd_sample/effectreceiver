#include "udp.h"

/*
#include <ctype.h>
#include <fcntl.h>
*/

#ifdef _WIN32
//#define  _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:4127)
#include <winsock2.h>
#include <mstcpip.h>
#else
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#endif

struct udp_conn {
#ifdef _WIN32
    SOCKET h;
#else
    int h;
#endif
    struct sockaddr_in addr;
};

struct udp_conn *udp_conn_new(const char *target_ip, int target_port, const char *bind_ip, int listen_port) {
    struct udp_conn r, *res;
#ifdef _WIN32
    static int WSASTARTUP_CALLED = 0;
    if (!WSASTARTUP_CALLED) {
        WORD VersionRequested = MAKEWORD(2,2);
        WSADATA wsaData;
        WSASTARTUP_CALLED = 1;
        WSAStartup(VersionRequested, &wsaData);
    }
#endif

    //#ifdef _WIN32
    //		WSAIoctl(h, SIO_UDP_CONNRESET, &bNewBehavior, sizeof(bNewBehavior), NULL, 0, &dwBytesReturned, NULL, NULL);
    //#endif

    if (target_ip != NULL && target_port > 0) {
        r.addr.sin_family = AF_INET;
        r.addr.sin_addr.s_addr = inet_addr(target_ip);
        if (r.addr.sin_addr.s_addr == INADDR_NONE)
            return NULL;
        r.addr.sin_port = htons((unsigned short)target_port);
    }
    r.h = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (listen_port > 0) {
        struct sockaddr_in addr;
        memset(&addr, 0, sizeof(struct sockaddr_in));
        addr.sin_family      = AF_INET;
        addr.sin_addr.s_addr = ((bind_ip == NULL) ? htonl(INADDR_ANY) : inet_addr(bind_ip));
        addr.sin_port        = htons((unsigned short)listen_port);
        if ((bind(r.h, (struct sockaddr *)&addr, sizeof(struct sockaddr_in))) < 0) {
#ifdef _WIN32
            closesocket(r.h);
#else
            close(r.h);
#endif
            return NULL;
        }
    }

    res = (struct udp_conn *)malloc(sizeof(struct udp_conn));
    memcpy(res, &r, sizeof(r));
    return res;
}

void udp_conn_close(struct udp_conn *h) {
    if (!h)
        return;

    if (h->h) {
#ifdef _WIN32
        shutdown(h->h, SD_BOTH);
        closesocket(h->h);
        h->h = INVALID_SOCKET;
#else
        shutdown(h->h, SHUT_RDWR);
        close(h->h);
        h->h = 0;
#endif
    }

    free(h);
}

int udp_conn_send(struct udp_conn *h, const char *buf, int buflen) {
    int r = sendto(h->h, buf, buflen, 0, (struct sockaddr *)&(h->addr), sizeof(struct sockaddr_in));
#ifndef _WIN32
    if (r < 0 && (errno == EAGAIN || errno == EWOULDBLOCK))
        return 0;
#endif
    return r;
}

int udp_conn_recv(struct udp_conn *h, unsigned char *buf, int buflen, long timeout_us, struct in_addr *addr, uint16_t *port) {
    int r;
    struct sockaddr_in sa;
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(h->h, &fds);

    if (timeout_us >= 0) {
        struct timeval tv;
        tv.tv_sec  = timeout_us / 1000000;
        tv.tv_usec = timeout_us % 1000000;
        r = select(h->h+1, &fds, 0, 0, &tv);
        if (r <= 0)
            return r; // timeout or error
    }
    {
#ifdef _WIN32
    int salen = sizeof(struct sockaddr_in);
    r = recvfrom(h->h, (char *)buf, buflen, 0, (struct sockaddr *)&sa, &salen);
#else
    socklen_t salen = sizeof(struct sockaddr_in);
    r = recvfrom(h->h, (char *)buf, buflen, 0, (struct sockaddr *)&sa, &salen);
    if (r < 0 && (errno == EAGAIN || errno == EWOULDBLOCK))
        return 0;
    if (r == 0)
        return -1; // socket closed
#endif
    }

    if (addr)
        *addr = sa.sin_addr;

    if (port)
        *port = ntohs(sa.sin_port);

    return r;
}

