#include "adam.h"

#include <stdlib.h>
#include <string.h>

#include "udp.h"

#define PACKET_MAXLEN (4096)
#define MAGIC_NUM (0x4d44414d) // MADM

#define HEX(X) (((X)>=10)?((X)-10+'A'):((X)+'0'))

struct adam {
    struct udp_conn *h;
    unsigned char rx[PACKET_MAXLEN];
};

struct adam_stream {
    uint32_t magic;
    uint8_t  func;
    uint8_t  len;
    uint16_t di_data;
    uint16_t do_data;
};

struct adam *adam_new(const char *bind_ip, int self_listen_port, const char *target_ip, int target_port) {
    struct udp_conn *h = NULL;
    struct adam *r = NULL;

    h = udp_conn_new(target_ip, target_port, bind_ip, self_listen_port);
    if (!h)
        return NULL;

    r = (struct adam*)malloc(sizeof(struct adam));
    if (!r) {
        udp_conn_close(h);
        return NULL;
    }
    r->h = h;

    return r;
}
void adam_del(struct adam *h) {
    if (!h)
        return;
    udp_conn_close(h->h);
    free(h);
}

int adam_recv(struct adam *h, int timeout_us, struct adam_udp_msg *msg) {
    int c;
    struct adam_stream *m;
    struct in_addr addr;
    uint16_t port = 0;

    c = udp_conn_recv(h->h, h->rx, PACKET_MAXLEN, timeout_us, &addr, &port);
    if (c == 0)
        return 0;
    if (c < 0)
        return -1;

    m = (struct adam_stream *)h->rx;
    if (m->magic != MAGIC_NUM)
        return 0;

    if (msg) {
        msg->ip = addr.s_addr;
        msg->port = port;
        msg->di_val = m->di_data;
        msg->do_val = m->do_data;
    }

    return c;
}

int adam_set_do(struct adam *h, uint16_t do_val) {
    char cmd[128] = "#0100  \r";
    cmd[5] = (do_val & 0xf0) >> 4;
    cmd[5] = HEX(cmd[5]);
    cmd[6] = (do_val & 0x0f);
    cmd[6] = HEX(cmd[6]);

    return udp_conn_send(h->h, cmd, 8);
}

