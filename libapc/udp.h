#ifndef LIBAPC_UDP_H
#define LIBAPC_UDP_H

#ifdef _WIN32
#include <winsock2.h>
#else
#include <netinet/in.h>
#endif

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

struct udp_conn;

// ip: target ip address.
// port: target port.
// listen: local listen port.
// return NULL if failed
struct udp_conn *udp_conn_new(const char *target_ip, int target_port, const char *bind_ip, int listen_port);


void udp_conn_close(struct udp_conn *h);

// return: number of bytes sent.
//         < 0 error.
//         = 0 timeout.
int udp_conn_send(struct udp_conn *h, const char *buf, int buflen);

// timeout_us:  -1: blocking.
//             = 0: return immediately.
// return: number of bytes readed.
//         < 0 error.
//         = 0 timeout,
int udp_conn_recv(struct udp_conn *h, unsigned char *buf, int buflen, long timeout_us, struct in_addr *addr, uint16_t *port);

#ifdef __cplusplus
}
#endif

#endif // LIBAPC_UDP_H

