#ifndef LIBAPC_ADAM_H_
#define LIBAPC_ADAM_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

struct adam_udp_msg {
    uint32_t ip;
    uint16_t port;
    uint16_t di_val;
    uint16_t do_val;
};

struct adam;

struct adam *adam_new(const char *bind_ip, int self_listen_port, const char *target_ip, int target_port);

// timout_us: -1: blocking
// return: > 0: success, number of byte received.
//         = 0: timeout / format error / security check failed.
//         < 0: network error.
int adam_recv(struct adam *h, int timeout_us, struct adam_udp_msg *msg);

int adam_set_do(struct adam *h, uint16_t do_val);

void adam_del(struct adam *h);

#ifdef __cplusplus
}
#endif

#endif // LIBAPC_ADAM_H_

