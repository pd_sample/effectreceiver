#ifndef LIBAPC_APC_H_
#define LIBAPC_APC_H_

// generate aes key: openssl aes-128-cbc -P -k {password}

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define APC_MSG_NULL (0)
#define APC_MSG_MPC  (100)
#define APC_MSG_PLS  (300)
#define APC_MSG_CCS  (400)
#define APC_MSG_SFX  (500)

struct apc_msg_hdr_t {
    uint32_t magic;
    uint32_t len;
    uint32_t id;
    uint32_t type;
    uint32_t serial; // non-repeat serial
};

struct apc_sender;

struct apc_sender *apc_sender_new(
    int self_id, const char *target_ip, int target_port, 
    const char *bind_ip, const char *target_key_hex);

// return: > 0: success, number of byte sent.
//         = 0: timeout / data error / security error.
//         < 0: network error.
int apc_sender_send(struct apc_sender *h, struct apc_msg_hdr_t *msg);

void apc_sender_del(struct apc_sender *h);


struct apc_receiver;

struct apc_receiver *apc_receiver_new(const char *bind_ip, int self_listen_port, const char *self_key_hex);

// timout_us: -1: blocking
// return: > 0: success, number of byte received.
//         = 0: timeout / format error / security check failed.
//         < 0: network error.
int apc_receiver_recv(struct apc_receiver *h, int timeout_us, struct apc_msg_hdr_t **pmsg);

void apc_receiver_del(struct apc_receiver *h);

#ifdef __cplusplus
}
#endif

#endif // LIBAPC_APC_H_

