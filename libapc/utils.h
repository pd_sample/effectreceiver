#ifndef LIBAPC_UTILS_H
#define LIBAPC_UTILS_H

#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include <stdarg.h>

#ifdef __cplusplus
extern "C" {
#endif

int64_t get_time_us(void);

int read_file_content(const char *filename, unsigned char *buf, int buf_size);
int write_file_content(const char *filename, unsigned char *buf, int buf_size);

int read_pidfile(const char *filename);
void write_pidfile(const char *filename, int pid);

int str_has_prefix(const char *str, const char *prefix);

uint16_t crc16(uint8_t *buffer, uint16_t buffer_length);

#ifdef _WIN32

#define snprintf_s(a,b,c,...) _snprintf_s(a,b,_TRUNCATE,c,__VA_ARGS__)
#define vsnprintf_s(a,b,c,d) _vsnprintf_s(a,b,_TRUNCATE,c,d)

#else
typedef int errno_t;

errno_t strcpy_s(char *s1, size_t s1max, const char *s2);
errno_t fopen_s(FILE **pf, const char *filename, const char *mode);
int vsnprintf_s(char *buf, size_t buflen, const char *format, va_list arg);
int snprintf_s(char *buf, size_t buflen, const char *format, ...);

#endif

#ifdef __cplusplus
}
#endif

#endif // LIBAPC_UTILS_H

