#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bgt_motionfile.h"

void add() {
    char buf[20000];
    struct bgt_motion_file_header *hdr = (struct bgt_motion_file_header *)buf;

    memset(buf, 0, 20000);

    FILE *f1 = fopen("1.motion", "r");

    fseek(f1, 0, SEEK_END);
    int size = ftell(f1);
    rewind(f1);

    fread(buf+512, 1, size, f1);
    fclose(f1);


    FILE *f2 = fopen("11.motion", "w");
    memcpy(hdr->magic, BGT_MOTION_FILE_MAGIC, sizeof(hdr->magic));
    hdr->file_ver = 1;
    hdr->type = 1;
    strcpy(hdr->motion_name, "test motion");
    hdr->motion_ver = 1;
    hdr->mps = 6000;
    hdr->motion_num = size/12;

    bgt_motion_file_hash(hdr);

    fwrite(buf, 1, size+512, f2);
    fclose(f2);
}

void check() {
    char buf[20000];
    struct bgt_motion_file_header *hdr = (struct bgt_motion_file_header *)buf;

    memset(buf, 0, 20000);

    FILE *f1 = fopen("11.motion", "r");

    fseek(f1, 0, SEEK_END);
    int size = ftell(f1);
    rewind(f1);

    fread(buf, 1, size, f1);
    fclose(f1);

    printf("check: %d\n", bgt_motion_file_check(hdr));
}

int main() {

    printf("header size: %d\n", sizeof(struct bgt_motion_file_header));
    add();
    check();

    return 0;
}

