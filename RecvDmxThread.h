#pragma once

#include <QThread>
#include <QNetworkDatagram>



class RecvDmxThread : public QThread
{
    Q_OBJECT

public:
    RecvDmxThread(QObject *parent);
    ~RecvDmxThread();
    
protected:
    virtual void run();
    bool is_exit;

Q_SIGNALS:
    void DmxDataReceived(QNetworkDatagram data);
};
