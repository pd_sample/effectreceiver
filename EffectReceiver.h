#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_EffectReceiver.h"
#include <QNetworkDatagram>
#include <QMediaPlayer>
#include <QtMultimediaWidgets/qvideowidget.h>
#include <QDateTime>


#include "RecvDmxThread.h"
#include "sfx_msg.h"
#include <libapc\apc.h>

class EffectReceiver : public QMainWindow
{
    Q_OBJECT

public:
    EffectReceiver(QWidget *parent = Q_NULLPTR);
    ~EffectReceiver();

private:
    Ui::EffectReceiverClass ui;
    RecvDmxThread *recv_dmx_thread;

    int tc_played_time_ms, tc_start_time_ms;
    int tc_timer_id;

    void UpdateDMXUI(ArtDmx *dmx);
    void UpdateTimeCodeLabel(int time);
    void SendHttpRequest(bool is_play, int time = -1);

    apc_sender *tc_apc_sender;
    QList<QProgressBar*> bars;
protected:
    virtual void timerEvent(QTimerEvent *event);
private Q_SLOTS:
    void OnDmxDataRecevied(QNetworkDatagram data);
    void OnPlayBtnClicked();
    void OnStopBtnClicked();
};
