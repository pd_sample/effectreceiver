#ifndef SFX_MSG_H_
#define SFX_MSG_H_

#include "libapc/apc.h"
#define ART_NET  ("Art-Net\0")

typedef struct {
    char		id[8];      //"Art-Net"
    uint16_t	opCode;     // See Doc. Table 1 - OpCodes eg. 0x5000 OpOutput / OpDmx
    uint16_t	version;    // 0x0e00 (aka 14)
    uint8_t		seq;        // monotonic counter
    uint8_t		physical;   // 0~3(10), 決定哪個port來收封包
    uint8_t		subNet;     // low universe (0-255), 決定哪些裝置可以收到封包
    uint8_t		net;        // high universe (not used)
    uint8_t		lengthLo;    // data length (2 - 512)
    uint8_t		lengthHi;
    uint8_t		data[512];	// universe data
} ArtDmx;

enum t_mpc_msg_type {
    sfx_msg_indication = APC_MSG_SFX,
    sfx_msg_sync,
};

struct t_sfx_sync {
    struct apc_msg_hdr_t hdr;
    int32_t time_ms;          
};


#endif // SFX_MSG_H_
