#include "EffectReceiver.h"
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

#pragma comment (lib, "Ws2_32.lib")

#define LIBAPC_KEY_DEF_DEFAULT  ("AA335750E88079DF4216C700A5FDB012")
#define SYNC_TARGET_IP "127.0.0.1"

EffectReceiver::EffectReceiver(QWidget *parent)
    : QMainWindow(parent) {
    ui.setupUi(this);
    tc_apc_sender = NULL;
    recv_dmx_thread = new RecvDmxThread(this);
    connect(recv_dmx_thread, &RecvDmxThread::DmxDataReceived, this, &EffectReceiver::OnDmxDataRecevied);
    recv_dmx_thread->start();

    connect(ui.play_btn, &QPushButton::released, this, &EffectReceiver::OnPlayBtnClicked);
    connect(ui.stop_btn, &QPushButton::released, this, &EffectReceiver::OnStopBtnClicked);

    for (int i = 0; i <= 34; ++i) {
        auto bar = findChild<QProgressBar*>(QString("dmx_bar_%1").arg(QString::number(i)));
        if (bar) {
            bars.append(bar);
        }

    }

    
}



void EffectReceiver::OnDmxDataRecevied(QNetworkDatagram data) {
    QByteArray array = data.data();
    ArtDmx* dmx = (ArtDmx*)array.data();
    UpdateDMXUI(dmx);
}

EffectReceiver::~EffectReceiver() {
    recv_dmx_thread->terminate();
    recv_dmx_thread->deleteLater();
}

void EffectReceiver::UpdateDMXUI(ArtDmx *dmx) {

    if (dmx->subNet == ui.universal_num->toPlainText().toInt()) {
        for (int i = 0; i < bars.size(); ++i) {
            auto label = findChild<QLabel*>(QString("label_%1").arg(QString::number(i + 1)));
            bars[i]->setValue((int)dmx->data[label->text().toInt() - 1]);

        }
    }

}

void EffectReceiver::UpdateTimeCodeLabel(int time) {
    int sec, min, hour;
    sec = (time / 1000) % 60;
    min = (time / 1000 / 60) % 60;
    hour = time / 1000 / 60 / 60;

    QString time_format = QString().sprintf("%02d:%02d:%02d", hour, min, sec);
    ui.timecode_label->setText(time_format);
}
void EffectReceiver::SendHttpRequest(bool is_play, int time) {
    QNetworkAccessManager mgr;
    QString cmd;
    if (is_play) {
        cmd = QString().sprintf("http://127.0.0.1:9001/player/play?effect=Vinpearl&time=%d", time);
    } else {
        cmd = QString("http://127.0.0.1:9001/player/stop");

    }
    QEventLoop loop;
    QNetworkRequest request;
    request.setUrl(QUrl(cmd));
    QNetworkReply *reply = mgr.get(request);
    connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    loop.exec();

}
static int video_sync_time = 0;
void EffectReceiver::timerEvent(QTimerEvent * event) {

    if (event->timerId() == tc_timer_id) {
        int diff_time = QDateTime::currentMSecsSinceEpoch() - tc_start_time_ms;
        UpdateTimeCodeLabel(diff_time);

        if (tc_apc_sender) {
            t_sfx_sync sync_msg;
            sync_msg.hdr.len = sizeof(t_sfx_sync);
            sync_msg.hdr.type = sfx_msg_sync;
            sync_msg.time_ms = diff_time;
            apc_sender_send(tc_apc_sender, (apc_msg_hdr_t*)&sync_msg);
        }




    }
}

void EffectReceiver::OnPlayBtnClicked() {
    tc_start_time_ms = (QDateTime::currentMSecsSinceEpoch() - ui.jump_time->toPlainText().toInt());
    SendHttpRequest(true, tc_start_time_ms);
    tc_timer_id = startTimer(200);
    video_sync_time = 0;

    if (tc_apc_sender == NULL) {
        tc_apc_sender = apc_sender_new(1, SYNC_TARGET_IP, 10501, NULL, LIBAPC_KEY_DEF_DEFAULT);
    }

    ui.play_btn->setEnabled(false);
    ui.stop_btn->setEnabled(true);

}

void EffectReceiver::OnStopBtnClicked() {
    this->killTimer(tc_timer_id);

    SendHttpRequest(false);

    if (tc_apc_sender) {
        apc_sender_del(tc_apc_sender);
        tc_apc_sender = NULL;
    }
    ui.play_btn->setEnabled(true);
    ui.stop_btn->setEnabled(false);
}
